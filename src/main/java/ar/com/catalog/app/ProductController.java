package ar.com.catalog.app;

import ar.com.catalog.app.commons.Endpoints;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductController {

    @GetMapping(Endpoints.PRODUCTS)
    public List<String> getProducts() {
        return List.of("Notebook", "Monitor", "Teclado");
    }

}
